<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class UsersSharing extends Authenticatable
{
    protected  $table = 'users_sharing';
    protected $fillable=['us_user_id','us_user_to_id','us_amount'];

    public function user() {
        return $this->hasOne('App\User','id','us_user_id');
    }
    public function userto() {
        return $this->hasOne('App\User','id','us_user_to_id');
    }
}
