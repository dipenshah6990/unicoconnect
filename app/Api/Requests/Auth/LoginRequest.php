<?php

namespace App\Api\Requests\Auth;

use Dingo\Api\Http\FormRequest;
use Illuminate\Http\Exceptions\HttpResponseException;
use Illuminate\Contracts\Validation\Validator;

class LoginRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
//            "verification_method" => "bail|required|in:email,mobile",
            "email" => "bail|required_if:verification_method,email|sometimes|email",
//            "user_phone" => "bail|required_if:verification_method,mobile|sometimes|digits_between:9,16",
//            "user_phone_country_code" => "bail|required_if:verification_method,mobile|sometimes|digits_between:1,4",
            "password" => "required:string",
        ];
    }

    public function messages() {
        return [
//            "verification_method.required" => "Select validation method",
//            "verification_method.in" => "Validaiton method must be either email or mobile",
            "email.required_if" => "Email is a mandatory field",
            "email.email" => "Email address is invalid",
//            "user_phone.required_if" => "Select valid country code",
//            "user_phone.required_if" => "Enter valid mobile number",
            "password.required" => "Password is a mandatory field",
        ];
    }


    public function failedValidation(Validator $validator) {
        //write your bussiness logic here otherwise it will give same old JSON response
        $responseArr = ['response_code' => 200, 'success' => false, 'message' => $validator->errors()->first(), 'data' => []];
        throw new HttpResponseException(response()->json($responseArr , 200));
    }

}
