<?php

namespace App\Api\Controllers\Auth;

use \App\Api\Controllers\BaseApiController;
use App\Api\Requests\Auth\LoginRequest;
use App\User;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Token;

class LoginController extends BaseApiController {
    /*
      |--------------------------------------------------------------------------
      | Login Controller
      |--------------------------------------------------------------------------
      |
      | This controller handles authenticating users for the application and
      | redirecting them to your home screen. The controller uses a trait
      | to conveniently provide its functionality to your applications.
      |
     */

    use AuthenticatesUsers;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    protected function credentials(Request $request) {
        return ['email' => $request->get('email'), 'password' => $request->get('password')];
    }

    protected function getUser(Request $request) {
        $credentials = $this->credentials($request);
        return \App\User::where('email', $request->input('email'))->first();
    }

    public function login(LoginRequest $request) {
        try {

            $user = $this->getUser($request);
            if (!auth()->attempt($this->credentials($request))) {
                $data = array();
                return $this->ApiResponseError((object) $data, $this->getFailedLoginResponseText($user), 200);
            } else {

                $user = $request->user();
                $data_pass = $this->loginResponse($request, $user);

                return $this->ApiResponseSuccess($data_pass, 'Logged in Successfully', 200);
            }
        } catch (\Exception $ex) {
            $this->ApiResponseError([], $ex->getMessage(), 200);
        }
    }

    protected function validateLogin(Request $request) {
        $rules = [
            "email" => "bail|required_if:verification_method,email|sometimes|email",
            "password" => "required:string",
        ];

        $messages = [
            "email.required_if" => "Email is a mandatory field",
            "email.email" => "Email address is invalid",
            "password.required" => "Password is a mandatory field",
        ];

        $request->validate($rules, $messages);
    }

    protected function getFailedLoginResponseText($user) {
        if ($user) {
            $message = trans('auth.failed');
        } else {
            $message = trans('auth.invalid');
        }
        return $message;
    }

    protected function loginResponse($request, $user){
        try{
            // delete all other token from db for specific user
            Token::where('user_id', $user->id)->update(['revoked' => true]);
            $tokenResult = $user->createToken('Aspire API Access', ['*']);
            $token = $tokenResult->token;
            $token->save();

        }catch (\Exception $e){
            dd($e->getMessage());
        }
        $data_pass = $user;
        $data_pass['token_type'] = 'Bearer';
        $data_pass['access_token'] = $tokenResult->accessToken;
        return $data_pass;
    }
}
