<?php

namespace App\Api\Controllers\User;

use \App\Api\Controllers\BaseApiController;
use App\Api\Requests\Auth\LoginRequest;
use App\Loan;
use App\User;
use App\UsersSharing;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Token;

class UserController extends BaseApiController {

    public function index(Request $request){
        dd($request->all());
    }

    /***
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @define insert new loan record for login user
     */
    public function create(Request $request){
        /* validate if request has valid data or not */
        try{
            $request->validate([
                "name" => "required",
                "email" => "required|email",
                "mobile_number" => "required|digits:10"
            ]);
        }catch (\Exception $e){
            $error_message='';
            foreach ($e->errors() as $error){
                if($error_message=='')
                    $error_message=(implode(',',$error));
                else
                    $error_message.=','.(implode(',',$error));
            }
            return $this->ApiResponseError([], $error_message, 200);
        }

        /* create seperate entry for all user for given user */
        $loanData= User::create([
            "name"=>$request->name,
            "email"=>$request->email,
            "mobile_number"=>$request->mobile_number,
            'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
        ]);

        /* return new created loan response */
        return $this->ApiResponseSuccess($loanData, 'User added successfully', 200);
    }

    /***
     * get list of all user data with amount
     */
    public function list(){
        /* declare empty array to store user data */
        $userData= [];
        /* get all User sharing data */
        $usersSharingData = UsersSharing::select(DB::raw('sum(us_amount) as total'),'us_user_id','us_user_to_id')->groupBy(['us_user_id','us_user_to_id'])->get();
        foreach ($usersSharingData as $userSharingData){
            /* assign default value */
            $startAmount=number_format(0,2);
            $endAmount=number_format(0,2);
            $startName='';$endName='';

            /* assign value to variable */
            $startAmount=$userSharingData->total;
            $startName=$userSharingData->user->name;

            /* get if dependent data is exist or not */
            $dependentData=UsersSharing::select(DB::raw('sum(us_amount) as total'),'us_user_id','us_user_to_id')->groupBy(['us_user_id','us_user_to_id'])->where(['us_user_id'=>$userSharingData->us_user_to_id,'us_user_to_id'=>$userSharingData->us_user_id])->first();
            /* check if data is set or not if not then set user name in endName variable */
            if (isset($dependentData)) {
                /* assign value to variable */
                $endAmount=$dependentData->total;
                $endName=$dependentData->user->name;
            }else{
                $endName=$userSharingData->userto->name;
            }
            /* check if start amount is lower than end amount */
            if($startAmount < $endAmount){
                $userData[$startName.' owns '.$endName]=number_format((int)($startAmount-$endAmount),2) . '  ('.$startAmount.' - '.$endAmount.')';
            }else{
                $userData[$endName.' owns '.$startName]=number_format((int)($startAmount-$endAmount),2). '  ('.$startAmount.' - '.$endAmount.')';
            }
        }
        /* return user data with value */
        dd($userData);
    }
}
