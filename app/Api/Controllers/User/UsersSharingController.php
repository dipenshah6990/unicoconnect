<?php

namespace App\Api\Controllers\User;

use \App\Api\Controllers\BaseApiController;
use App\Api\Requests\Auth\LoginRequest;
use App\Loan;
use App\User;
use App\UsersSharing;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Laravel\Passport\Token;

class UsersSharingController extends BaseApiController {

    public function index(Request $request){
        dd($request->all());
    }

    /***
     * @param Request $request
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\Routing\ResponseFactory|\Illuminate\Http\Response
     * @define insert new loan record for login user
     */
    public function create(Request $request){
        /* validate if request has valid data or not */
        try{
            $request->validate([
                "us_amount" => "required",
                "type" => "required|in:EQUAL,EXACT,PERCENT,equal,exact,percent",
                "total_users" => "required",
                "user_ids" => "required_if:type,EXACT,PERCENT,exact,percent",
                "user_wise_value" => "required_if:type,EXACT,PERCENT,exact,percent"
            ]);
        }catch (\Exception $e){
            $error_message='';
            foreach ($e->errors() as $error){
                if($error_message=='')
                    $error_message=(implode(',',$error));
                else
                    $error_message.=','.(implode(',',$error));
            }
            return $this->ApiResponseError([], $error_message, 200);
        }
        $amount = 0;
        if(strtolower($request->type) == 'equal'){
            $amount = number_format(($request->us_amount/$request->total_users),2);
        }
        $cnt = 0;
        $userIds = is_array($request->user_ids) ? $request->user_ids : explode(',',$request->user_ids);
        $user_wise_value = is_array($request->user_wise_value) ? $request->user_wise_value : explode(',',$request->user_wise_value);

        foreach ($userIds as $userId) {
            if(strtolower($request->type) == 'exact'){
                $amount = array_key_exists($cnt,$user_wise_value) ? $user_wise_value[$cnt] : 0;
            }
            if(strtolower($request->type) == 'percent'){
                $amount = array_key_exists($cnt,$user_wise_value) ? number_format((($request->us_amount*$user_wise_value[$cnt])/100),2) : 0;
            }
            if(auth()->id() != $userId) {
                $userSharingData = UsersSharing::create([
                    "us_user_id" => auth()->id(),
                    "us_user_to_id" => $userId,
                    "us_amount" => $amount
                ]);
            }
            $cnt++;
        }

        /* return new created loan response */
        return $this->ApiResponseSuccess($userSharingData, 'User added successfully', 200);
    }
}
