# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

create db with name : unicoconnect

rename .env.examlpe to : .env

add bellow details in .env
	DB_DATABASE=unicoconnect
	DB_USERNAME=root
	DB_PASSWORD=root
	API_PREFIX=api
	API_SUBTYPE=app
	API_VERSION=v1

composer install

php artisan migrate

php artisan db:seed

php artisan passport:install

php artisan key:generate

php artisan config:cache

php artisan serve 

### Default Login Credential

default user credential is for login: 
email : uesr1@yopmail.com
password : 12345678


### API LINK

1. For login 
	api : http://127.0.0.1:8000/api/V1/auth/login
	parameter : email:user1@yopmail.com
	            password:12345678
	type : post

2. For create user
	api : http://127.0.0.1:8000/api/V1/user/create
	parameter : name:user4
		    email:user4@yopmail.com
		    mobile_number:8469909184
    	type : post
	authorization : OAuth2.0 (insert token from login api response)

3. For create user sharing
	api : http://127.0.0.1:8000/api/V1/user-sharing/create
	parameter : user_ids:1,2,3,4 (comma seperated)
		    us_amount:1200
		    type:PERCENT (EQUAL,EXACT,PERCENT)
		    user_wise_value:40,20,20,20 (comma seperated)
		    total_users:4
	type : post
	authorization : OAuth2.0 (insert token from login api response)
	
4. For get user list
	api : http://127.0.0.1:8000/api/V1/user/list
	authorization : OAuth2.0 (insert token from login api response)
	type : post
