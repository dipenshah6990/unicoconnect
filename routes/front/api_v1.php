<?php
$api->group(['prefix' => 'auth'], function ($api) {
    $api->post('login', ['as' => 'login','uses' => 'Auth\LoginController@login']);
});
$api->group(['middleware' => 'auth:api'], function ($api) {
    $api->group(['prefix' => 'user'], function ($api) {
        $api->post('/create', 'User\UserController@create');
        $api->post('/list', 'User\UserController@list');
    });
    $api->group(['prefix' => 'user-sharing'], function ($api) {
        $api->post('/create', 'User\UsersSharingController@create');
    });
});
