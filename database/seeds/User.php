<?php

use Illuminate\Database\Seeder;

class User extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $records = $this->records();
        foreach ($records as $record) {
            \Illuminate\Support\Facades\DB::table('users')->insert([
                'name' => $record['name'],
                'email' => $record['email'],
                'password' => $record['password'],
                'mobile_number' => $record['mobile_number'],
                'created_at' => \Carbon\Carbon::now(),
                'updated_at' => \Carbon\Carbon::now(),
            ]);
        }
    }
    protected function records() {
        return [
            ['name' => 'User 1',
             'email' => 'user1@yopmail.com',
             'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
             'mobile_number' => '8469909181',
            ],
            ['name' => 'User 2',
                'email' => 'user2@yopmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'mobile_number' => '8469909182',
            ],
            ['name' => 'User 3',
                'email' => 'user3@yopmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'mobile_number' => '8469909183',
            ],
            ['name' => 'User 4',
                'email' => 'user4@yopmail.com',
                'password' => \Illuminate\Support\Facades\Hash::make('12345678'),
                'mobile_number' => '8469909184',
            ],
        ];
    }
}
